import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import NavBar from "./compoments/navbar/navbar";
import NavBarItem from "./compoments/navbar/item";

import ArticleList from './pages/article-list';
import ArticleDetail from './pages/article-detail';
import Home from './pages/home';
import Main from './compoments/main';

export default function App() {
  return (
    <Router>
      <div>
        <NavBar left='logo'>
          <NavBarItem><Link to="/">Home</Link></NavBarItem>
          <NavBarItem><Link to="/articles">About</Link></NavBarItem>
          <NavBarItem><Link to="/articles">Block</Link></NavBarItem>
          <NavBarItem><Link to="/articles">Contact</Link></NavBarItem>
        </NavBar>

        <Main>
          <Switch>
            <Route path="/articles">
              <ArticleList />
            </Route>
            <Route path="/articledetail">
              <ArticleDetail />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Main>
      </div>
    </Router>
  );
}
