import React from 'react';
import './item.scss';

function NavBarItem({children}) {
    return (
        <li className='navbar-item'>
            {children}
        </ li>
    );
}

export default NavBarItem;