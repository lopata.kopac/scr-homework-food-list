import React, {useState} from 'react';

import './navbar.scss';

function NavBar({children, left}) {
    const [collabsed, setColabsed] = useState(false);

    const onColabseHandle = () =>{
        setColabsed(!collabsed);
    }
    
    return (
        <nav  className='navbar-navbar'>
            <div className='navbar-iner'>
                <span>{left}</span>
                <button className={`navbar-mobile-button ${collabsed ? 'navbar-mobile-button-activate': ''}`}
                onClick={onColabseHandle}>
                    <span className="navbar-button-bar"></span>
                    <span className="navbar-button-bar"></span>
                    <span className="navbar-button-bar"></span>
                </button>
            </ div>
            <div className={`navbar-items-list ${collabsed ? 'navbar-items-list-active': ''}`}>
                <ul>{children}</ul>
            </div>
        </ nav>
    );
}

export default NavBar;