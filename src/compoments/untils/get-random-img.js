const imagelist =[
    'https://www.vitarian.sk/assets/images/clanky/vyziva/2016/ktore-jedlo-je-dobre/ktore-jedlo-je-dobre.jpg',
    'https://prod-wolt-venue-images-cdn.wolt.com/5e26edb13dd7f968ad78d74b/3ca12458-3eb7-11ea-bf5d-0a586473325b_IMG_6761.jpg',
    'https://staratrznica.sk/uploads/fck/image/do%20textu%20%284%29.jpg',
    'https://media.istockphoto.com/photos/top-view-table-full-of-food-picture-id1220017909?b=1&k=6&m=1220017909&s=170667a&w=0&h=yqVHUpGRq-vldcbdMjSbaDV9j52Vq8AaGUNpYBGklXs=',
    'https://media.istockphoto.com/photos/food-backgrounds-table-filled-with-large-variety-of-food-picture-id1155240408?k=6&m=1155240408&s=612x612&w=0&h=SEhOUzsexrBBtRrdaLWNB6Zub65Dnyjk7vVrTk1KQSU='
]


export default function getRandomImg(){
    return imagelist[Math.floor(Math.random() * imagelist.length)];
}