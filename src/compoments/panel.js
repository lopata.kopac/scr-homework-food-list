import styled from "styled-components";
import PropTypes from 'prop-types';

const Panel = styled.div`
    background: #F5F5DC;
    margin: 10px 0px;
    padding: ${props => props.type === 'large' ? '60px 12vw' : '10px'}
`;

Panel.propTypes = {
    type: PropTypes.oneOf(['large', 'small'])
};

Panel.defaultProps = {
    type: 'small'
};

export default Panel;